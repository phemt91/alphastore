import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  //messaggio = "Ciao a tutti sono il component Welcome"
  fakepasw  = "cXVlc3Rhbm9uZXVuYXBhc3N3b3Jk"
  saluti  = "Benvenuti in Alphastore"
  titolo2 = 'Seleziona gli articoli'

  utente =''



  constructor( private route : ActivatedRoute ) { }

  ngOnInit() {

    console.log("Totally useless " + this.fakepasw);
    this.utente = this.route.snapshot.params['userId']

  }

}
