import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WelcomeComponent } from '../welcome/welcome.component';
import { AuthappService } from '../services/authapp.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userId = ''
  password = ''
  autenticato = true
  //consentito = false
  errorMsg = 'Spiacente, user o password errati!'
  //infoMsg = 'Accesso Consentito'

  constructor(private route : Router, private BasicAuth: AuthappService) { }

  ngOnInit() {
  }

  gestAut() {
    if (this.BasicAuth.autentica(this.userId,this.password))
    {
      this.autenticato = true;
      this.route.navigate(['welcome', this.userId])
    }
    else
    {
      this.autenticato = false;
    }
    }
}



  //   if (this.userId === 'phemt' && this.password === '1234') {
  //       this.autenticato = true;
  //       this.route.navigate(['welcome', this.userId])
  //       //this.consentito = true;
  //   }
  //   else {
  //       this.autenticato = false;
  //       //this.consentito = false;
  //   }
  //
  // }
